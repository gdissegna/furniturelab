@extends('master')

@section('content')

    <!-- HEADER -->


    <a id="openNav">
        <div class="logointerno"><img src="{{ asset('images/etichetta.png') }}"  alt=""> </div>
    </a>

    <!-- END HEADER -->

    <div class="container">
        <h1 class="main_title">Cookies</h1>
        <div class="singular">
            <h2><strong>Use of cookies</strong></h2>
            <p>Furniture Lab uses cookies to make our services simple and efficient for users. Users viewing the Site, will enter the small amounts of information in the devices in use, which are computers and mobile devices, in small text files called &#8220;cookies&#8221; stored in the directories used by the Web Browser User.<br /> There are several types of cookies, some to make more effective use of the Site, other to enable certain functionality.<br /> Analyzing in detail our cookies allow you to:</p>
            <ul>
                <li>store the preferences</li>
                <li>avoid re-enter the same information several times during the visit such as user name and password</li>
                <li>To evaluate the use of the services and content provided by Furniture Lab to optimize the browsing experience and services offered.</li>
            </ul>
            <h2><strong>Types of cookies used by Furniture Lab</strong></h2>
            <p>Following are the various types of cookies used by Furniture Lab depending on the purpose of use</p>
            <h3><strong>Technical Cookies</strong></h3>
            <p>This type of cookie allows the proper operation of certain sections of the site. They are of two categories: persistent and session:</p>
            <ul>
                <li>persistent: once closed the browser are not destroyed but remain up to a preset expiration date</li>
                <li>Sessions: are destroyed each time the browser is closed</li>
            </ul>
            <p>These cookies, always sent from our domain, are required to view the site and in relation to technical services offered, will be increasingly used and then sent, unless the user does not change the settings in your browser (thus affecting the display of site pages).</p>
            <h3><strong>Analytical cookies</strong></h3>
            <p>Cookies in this category are used to collect information about the site Furniture Lab will use this information about anonymous statistical analysis in order to improve the use of the Site and to make the content more interesting and relevant to the desires of the users. This type of cookie collects anonymous data on the activity of users and how it arrived on the Site. The analytical cookies are sent from the same site or from third-party domains.</p>
            <h3><strong>Cookie analysis of third-party services</strong></h3>
            <p>These cookies are used to collect information on the use of the Site by users in anonymous form such as pages visited, time spent, traffic sources of origin, geographic origin, age, gender and interests for the purpose of marketing campaigns. These cookies are sent from third-party domains external to the Site.</p>
            <h3><strong>Cookies to integrate products and functions of third-party software</strong></h3>
            <p>This type of cookie integrates features developed by third parties within the pages of the Site such as icons and preferences expressed in social networks in order to share the contents of the site or for the use of services, third-party software (such as software generate maps and additional software that offer additional services). These cookies are sent from third-party domains and partner sites that offer their functionality through the pages of the Site.</p>
            <h3><strong>Cookie profiling</strong></h3>
            <p>Those cookies are needed to create user profiles in order to send advertising messages in line with the preferences expressed by the user within the pages of the Site.<br /> <span style="text-decoration: underline;">Furniture Lab does not use cookies profiling</span>.</p>
            <p>Furniture Lab, according to current legislation, it is not required to seek consent for cookies and technical analytics, as necessary to provide the required services.</p>
            <p>For all other types of cookies consent may be expressed by the user with one or more of the following modes:</p>
            <ul>
                <li>Using specific configurations of the browser or the related programs used to navigate the pages that make up the site.</li>
                <li>Through changing the settings in the use of third-party services</li>
            </ul>
            <p>Both of these solutions may prevent the user to use or display parts of the Site.</p>
            <h3><strong>Web sites and third-party services</strong></h3>
            <p>The Site may contain links to other Web sites that have their own privacy policy which can be different from the one adopted by Furniture Lab and therefore not responsible for these sites.</p>
        </div>

    </div>


@endsection