@extends('master')

@section('content')

    <!-- HEADER -->
    <a id="openNav">
        <div class="logointerno" style="top:0px"><img src="{{ asset('images/etichetta.png') }}" alt=""></div>
    </a>

    <div class="row margin-bottom-20" style="">
        <div class="col-md-5 col-md-offset-1 contact-container">
            <h1>
                Furniture Lab <br> di Laura Purinan & C. sas
            </h1>
            <p>
                Via I° Maggio, 7/5<br>
                33040 Corno di Rosazzo (UD)<br>
                Ph. +39 0432 1450948<br>
                Email: <a href="mailto:info@furniturelab.it">info@furniturelab.it</a><br>
                Web: www.furniturelab.it<br>
            </p>
        </div>
        <div class="col-md-6">
            <img src="{{ asset('images/contact/furniturelab-contact-us.jpg') }}">
        </div>
    </div>



@endsection
