@extends('master')

@section('content')

    <!-- HEADER -->
    <a id="openNav">
        <div class="logointerno" style="top:0px"><img src="{{ asset('images/etichetta.png') }}"  alt=""> </div>
    </a>



    <!-- END HEADER -->
    <!-- START what we do title -->
<div class="container-fluid">
    <div class="row">
        <div id="what-we-do" class="col-md-12 special_results_title margin-bottom-20">
            <div class="row">
                <div class="col-md-6 title-container">
                    <h1>WHAT WE DO</h1>
                    <p>our team</p>

                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-1.jpg') }}">
                </div>
            </div>
        </div>
    </div>
    <!-- END what we dop title title -->
    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-12">
                    <p class="what-we-do-longtext">
                        The purpose of our Company is to focus all developing prototypes process and mass<br>
                        production of seats, tables, upholstery and furnishing accessories for hotels, restaurants,<br>
                        residences, cruise ships and the residential market. We work together with architects,<br>
                        planner and interior designers.<br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="overflow: hidden;">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-12">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-2.jpg') }} " >
                </div>
            </div>
        </div>
    </div>
    <!-- START what we do title -->
    <div class="row">
        <div id="production" class="col-md-12 special_results_title margin-bottom-20">
            <div class="row">
                <div class="col-md-6">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-3.jpg') }}">
                </div>
                <div class="col-md-6 title-container production_title">
                    <h1>PRODUCTION</h1>
                    <p>concept takes shapes</p>
                </div>
            </div>
        </div>
    </div>
    <!-- END what we dop title title -->

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <p class="what-we-do-text what_we_do_text4" >
                        We believe in production's synergies and teamwork.
                        Products are shaped by many people working all together with skill, passion, craftmanshift and commitment to offer a vast selection of models.
                    </p>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4 ">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-4.jpg') }}">
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

    <div class="row" style="overflow: hidden;">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-12">
                    <img class="center-block img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-5.jpg') }} " >
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <p class="what-we-do-text what_we_do_text6">
                        We carefully check all the production phases, the quality of the materials, using the best technologies and processes , in respect of our artisan tradition with a strictly Italian workforce,  aimed to obtain the best result for our customers.
                    </p>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4 ">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-6.jpg') }}">
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-12">
                    <img class="center-block img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-7.jpg') }} " >
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-8.jpg') }}">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4 ">
                    <p class="what-we-do-text what_we_do_text8">
                        We dedicate special attention not only to detail, but also to service through constant communication with our customers.
                    </p>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

    <!-- START what we do title -->
    <div class="row">
        <div id="materials" class="col-md-12 special_results_title margin-bottom-20">
            <div class="row">
                <div class="col-md-6 title-container materials_title">
                    <h1>MATERIALS</h1>
                    <p>for durable furniture</p>
                </div>
                <div class="col-md-6">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-9.jpg') }}">
                </div>
            </div>
        </div>
    </div>
    <!-- END what we dop title title -->

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-10.jpg') }}">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4 ">
                    <p class="what-we-do-text what_we_do_text10">
                        Metal profiles and tubes stainless steel products in various shapes and dimensions.<br>
                    </p>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-12">
                    <img class="center-block img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-11.jpg') }} " >
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <p class="what-we-do-text what_we_do_text12">
                        Different species of wood like beech, ash, oak or walnut.
                    </p>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4 ">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-12.jpg') }}">
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-12 margin-bottom-20">
                    <img class="center-block img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-13.jpg') }} " >
                </div>
                <div class="col-md-12">
                    <p class="what-we-do-text text-center">
                        Several kind of shaped foams to ensure the best comfort.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 margin-bottom-20">
            <div class="row">
                <div class="col-md-4 col-md-offset-1">
                    <img class="img-responsive" src="{{ asset('images/what-we-do/furniture-what-we-do-14.jpg') }}">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-4 ">
                    <p class="what-we-do-text what_we_do_text14">
                        Fabrics, leather, velours, wool or customers' own material.
                    </p>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </div>


</div>

 @include('partials.footer')
@endsection

