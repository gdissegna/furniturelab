<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="description" content="Woody - Responsive Coming Soon Page">
    <meta name="keywords" content="Woody,  coming soon page, html5, responsive">
    <meta name="author" content="MountainTheme">

    <title>FurnitureLab - Imagine Design Create</title>

    <!-- ============ GOOGLE FONTS ============ -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css" />
    <!-- Bootstrap v3.3.1 -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- Custom styles CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">

</head>

<body>

<!-- LOADER TEMPLATE -->
<div id="page-loader">
    <div class="loader-icon fa fa-spin colored-border"></div>
</div>
<!-- /LOADER TEMPLATE -->



<!-- HEADER -->
<header>

    <a id="openNav">
        <div class="logo"><img src="images/etichetta.jpg"  alt=""> </div>
    </a>
</header>
<!-- END HEADER -->

<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">

            <div class="copyright">YOU WILL LOVE OUR PRODUCTS AS MUCH AS WE DO</div>

        </div>
    </div>
</footer>
<!-- END FOOTER -->


<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a>
    <div class="overlay-content">
        <a href="#">HOME</a>
        <a href="#">WHAT WE DO</a>
        <a href="#">MATERIALS</a>
        <a href="#">SPECIAL RESULTS</a>
        <a href="#">CONTACT US</a>
    </div>
</div>


<!-- Javascript files -->
<!-- jQuery -->
<script src="js/jquery.js"></script>
<!-- Backstretch -->
<script src="js/jquery.backstretch.min.js"></script>
<!-- CountDown  -->
<script src="js/jquery.countdown.js"></script>
<!-- Validate form -->
<script src="js/jquery.validate.js"></script>
<!-- Smooth Scroll -->
<script src="js/smooth-scroll.js"></script>
<!-- Youtube Player -->
<script src="js/jquery.youtubebackground.js"></script>
<!-- Ajax chimp -->
<script src="js/jquery.ajaxchimp.js"></script>
<!-- Google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl-EDTJ5_uU3zBIX7-wNTu-qSZr1DO5Dw"></script>
<!-- jQuery Tweetie -->
<script src="js/tweetie.js"></script>
<!-- Custom -->
<script src="js/main.js"></script>
</body>
</html>