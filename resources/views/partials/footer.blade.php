

<footer class="footer">
    <div class="row">
        <div class="col-md-2 col-xs-2">
            <img src="images/etichetta.jpg" class="logofooter" alt="Marchio FurnitureLAB">
        </div>
        <div class="col-md-8 col-xs-8">
            <p class="testo">
                Furniture Lab di Laura Purinan & C. sas<br>
                Via I° Maggio, 7/5<br>
                33040 Corno di Rosazzo (UD)<br>
                Ph. +39 0432 1450948<br>

                E-Mail: info@furniturelab.it<br>
                Web: www.furniturelab.it<br>
            </p>
        </div>
        <div class="col-md-2 col-xs-2 privacy">
            <a href="{{ route('privacy') }}">Privacy</a> - <a href="{{ route('cookie') }}">cookies</a>
        </div>
    </div>
</footer>