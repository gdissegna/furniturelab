@extends('master')

@section('content')

    <!-- HEADER -->


    <a id="openNav">
        <div class="logointerno"><img src="{{ asset('images/etichetta.png') }}"  alt=""> </div>
    </a>

    <!-- END HEADER -->

    <div class="container">

        <h1 class="main_title">Privacy Policy</h1>
        <div class="singular">
            <p>Dear Customer,</p>
            <p>Pursuant to articles 12, 13, and 14 of the European Regulation n. 679/2016, we hereby provide you with the Information Notice on the Processing of Your Personal Data.</p>
            <p>We remind you that you have the right to read it before transmitting your Personal Data to us, as well as the right to object to one or more purposes identified in this Information Notice at the relevant section.</p>
            <p>The Data Controller is:</p>
            <p>
                <strong>Furniture Lab di Laura Purinan & C. sas</strong><br />
                <strong>Via I° Maggio, 7/5</strong><br />
                <strong>33040 Corno di Rosazzo (UD)</strong><br />
                <strong>Phone + 39 0432 1450948</strong><br />
                <strong>E-mail: <a href="mailto:info@furniturelab.it">info@furniturelab.it</a></strong><br />
                <strong> P. IVA IT02250840309</strong></p>


            <p>The main processing activities carried out are related to the fulfillment of the purchase agreement,the support contract and to all obligations referred to such contracts (whether of a contractual or fiscal nature). Furthermore, we process your Data for the purpose of acquiring new clients and promoting our products, by means of marketing activity or registration to our website.</p>
            <p>The main risk associated with the above mentioned processing activities may be the possibility that one or more actions aimed at keeping the acquired clients may affect the rights and freedoms of Data Subjects.<br /> For example, the latter may be dissatisfied to receive unsolicited commercial offers.<br /> However, Furniture Lab di Laura Purinan & C. sas adopts all the technical and organizational measures necessary to ensure that those who will receive advertising materials or its commercial offers have actually given their consent.<br /> Furthermore, Furniture Lab di Laura Purinan & C. sas will make sure that the sending frequency and the chosen methods do not exceed what is considered to be a legitimate advertising activity.</p>
            <p>Usually, an identifiable risk is the possibility that non-authorized persons or third parties access Personal Data of which Furniture Lab di Laura Purinan & C. sas is the Data Controller of. However, pursuant to the Applicable Law, Furniture Lab di Laura Purinan & C. sas adopts all the appropriate technical and organizational measures in order to reduce such risk to the lowest acceptable level.</p>
            <p>&nbsp;</p>
            <p><strong>The purposes for which we process your Personal Data are the following</strong></p>
            <ol>
                <li>If you are a Customer of Furniture Lab di Laura Purinan & C. sas: Fulfillment of the obligations related to the purchase agreement and the support contract signed with the Data Subject and performance of all the pre-contractual measures referred to it, such as cost estimation, negotiation, etc. In this case, Consent is not required since the Legal Basis of the Data Processing is the contract signed with the Data Subject and all the pre-contractual measures related to it, or the Legitimate Interest of the Controller.</li>
                <li>If you are a natural person and you are not a customer of Furniture Lab di Laura Purinan & C. sas: Marketing activity by sending of commercial communications by means of email, telephone or postal service. In this case, Consent is required and constitutes the Legal Base of the processing itself.</li>
                <li>Management of the existing customer portfolio, including natural person retailers. In this case, the Legal Basis of the Data Processing is the Contract signed by the parties or the Legitimate Interest of the Controller. Therefore, neither the Consent nor the Legitimate Interest are required since “there is a relevant and appropriate relationship” with the Data Subject.</li>
            </ol>
            <p>Upon request, a comprehensive list of the Recipients, which we may need to share your Personal Data with in accordance with the applicable law, is available. Such request has to be submitted to the Data Controller without any particular formality.</p>
            <p>&nbsp;</p>
            <p><strong>The Categories of Recipients to which your Personal Data will be communicated to are the following:</strong></p>
            <ol>
                <li>Companies and Experts responsible for the management, maintenance and remote support of the IT system of Furniture Lab di Laura Purinan & C. sas</li>
                <li>Experts in the Accounting sector.</li>
                <li>Companies which manage the web systems of Furniture Lab di Laura Purinan & C. sas</li>
                <li>Software Houses and Producers of Software Solutions used by Furniture Lab di Laura Purinan & C. sas for the Processing of Personal Data.</li>
                <li>Public entities such as Income Revenue Authority, Italian Finance Police (Guardia di Finanza), etc. if the communication of your Personal Data is mandatory by Law.</li>
                <li>Banks.</li>
            </ol>
            <p>All the Recipients listed above will be appointed as Data Processors and they will be instructed by Furniture Lab di Laura Purinan & C. sas in accordance with legal provisions. Also, they are obliged to maintain confidentiality on the information submitted to them and to return or delete such information at the end of the processing activities.</p>
            <p>Furniture Lab di Laura Purinan & C. sas does not transfer and will not transfer your Personal Data to international organizations or Data Controllers located outside the European Economic Area. If Furniture Lab di Laura Purinan & C. sas decides to do so, it will strictly comply with the provisions of the Regulation (EU) 679/2016 on the transfer of Personal Data abroad and will inform you before the transfer takes place in accordance with legal provisions.</p>
            <p><strong> </strong></p>
            <p><strong>Furniture Lab di Laura Purinan & C. sas will retain your Personal Data according to the following criteria:</strong></p>
            <ol>
                <li>In case of Personal Data Subject to obligations of retention set by Italian or European Law: within the deadlines set by the legislation, after which Personal Data will be deleted or made anonymous.</li>
                <li>In case of Personal Data of natural persons who are clients of Furniture Lab di Laura Purinan & C. sas which are processed for the purposes referred to in point 3) of the purposes of the Processing: until the Data Subject withdraws their Consent and in any case no later than 5 years from the given Consent.</li>
                <li>Data which are not subject to retention obligations established by Italian or European Law and which belongs to retailers (natural persons) of Furniture Lab di Laura Purinan & C. sas will be deleted or made anonymous when the contract expires.</li>
            </ol>
            <p>&nbsp;</p>
            <p><strong>Below you may find the rights you are entitled to pursuant to the European Regulation n. 679/2016. </strong></p>
            <p>In case you wish to enforce one or more of the following rights please contact Furniture Lab di Laura Purinan & C. sas (the contact details are listed above).</p>
            <p>The customer shall have the right to access their personal data the Company holds pursuant to art. 15 of the European Regulation n. 679/2016, and the Data Controller is required to inform the Data Subject about the purposes of the proceeding.</p>
            <p>Pursuant to art. 16 of the European Regulation n. 679/2016, the Data Subject shall have the right to obtain from the Data Controller the rectification of inaccurate personal data concerning them without undue delay.</p>
            <p>Taking into account the purposes of the processing, the Data Subject shall have the right to integrate incomplete personal data, also by means of a supplementary statement.</p>
            <p>The Data Subject shall have the right to obtain from the Data Controller restriction of processing where the grounds provided by article 18 of the European Regulation n. 679/2016 apply.</p>
            <p>The Data Subject shall have the right to obtain from the Data Controller the delation of their personal data without undue delay and the Data Controller shall have the obligation to erase personal data without undue delay where the grounds provided by article 17 of the European Regulation n. 679/2016 apply.</p>
            <p>With regard to the purposes for which your Consent is required, <strong>you are entitled to withdraw the given Consent</strong>, i.e. to unilaterally terminate the processing activities related to the purposes for which you have given your Consent. The processing activities carried out before your Withdrawal will be in any case lawful, according to the applicable law.</p>
            <p>If you believe that a processing activity we have carried out has caused yourself an injury or damage, or if you believe that there has been an illegitimate processing of Personal Data, <strong>you are entitled to lodge a Complaint to the Supervisory Authority</strong>, according to the procedures established by the latter which are available on the following website:</p>
            <p><a href="http://www.garanteprivacy.it/home/diritti/come-agire-per-tutelare-i-nostri-dati-personali">http://www.garanteprivacy.it/home/diritti/come-agire-per-tutelare-i-nostri-dati-personali</a></p>
            <p><strong>If you refuse to provide Your Personal Data to Furniture Lab di Laura Purinan & C. sas, no purchase agreement or support contract will be signed between the interested parties.</strong></p>
            <p>In order to proceed to any of the above rights please send us an email to <strong><a href="mailto:info@furniturelab.it">info@furniturelab.it</a></strong>

    </div>


@endsection