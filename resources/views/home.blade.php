@extends('master')


@section('content')

    <!-- LOADER TEMPLATE -->
    <div id="page-loader">
        <div class="loader-icon fa fa-spin colored-border"></div>
    </div>
    <!-- /LOADER TEMPLATE -->

    <!-- HEADER -->
    <header>
        <a id="openNav">
            <div class="logo"><img src="images/etichetta.png"  alt=""> </div>
        </a>
    </header>
    <!-- END HEADER -->

    <!-- FOOTER -->
    <footer>
        <div class="container">
            <div class="row">

                <div class="copyright">YOU WILL LOVE OUR PRODUCTS AS MUCH AS WE DO</div>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->


@endsection


