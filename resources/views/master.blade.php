<!DOCTYPE HTML>
<html lang="en">

<head>

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta charset="UTF-8">
    <meta name="apple-touch-fullscreen" content="YES">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="title" content="Bespoke Furniture for hospitality Cruise Ships and residential" >
    <meta name="description" content="The purpose of our Company is to focus all developing prototypes process and mass
production of seats, tables, upholstery and furnishing accessories for hotels, restaurants, residences, cruise ships and the residential market. We work together with architects, planner and interior designers.">
    <meta name="keywords" content="Developing prototypes process, mass production of seats, tables, upholstery, furnishing accessories for hotels, furnishing accessories for restaurants, residences, cruise ships and the residential market">
    <meta name="author" content="Omega di Bellini enrico">
    <link rel="icon"
          type="image/png"
          href="images/favicon/favicon.png">
    <!-- ============ Favicon ============ -->
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>FurnitureLab - Bespoke Furniture for hospitality cruise ships and residential</title>

    <!-- ============ GOOGLE FONTS ============ -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600' rel='stylesheet' type='text/css'>


    <!-- CSS -->
    <!-- Animate css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome/css/font-awesome.min.css') }}" />
    <!-- Bootstrap v3.3.1 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Custom styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <!-- Favicons -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

</head>

<body>


@yield('content')

<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" id="closeNav">&times;</a>
    <div class="overlay-content">
        <a href="{{ route('home') }}">HOME</a>
        <a href="{{ route('what-we-do') }}#what-we-do">WHAT WE DO</a>
        <a href="{{ route('what-we-do') }}#production">PRODUCTION</a>
        <a href="{{ route('what-we-do') }}#materials">MATERIALS</a>
        <a href="{{ route('special-results') }}">SPECIAL RESULTS</a>
        <a href="{{ route('privacy') }}">PRIVACY STATEMENT</a>
        <a href="{{ route('notices') }}">NOTICES</a>
        <a href="{{ route('contact') }}">CONTACT US</a>

    </div>
</div>


<!-- Javascript files -->
<!-- jQuery -->
<script src="{{ asset('js/jquery.js') }}"></script>
<!-- Backstretch -->
<script src="{{ asset('js/jquery.backstretch.min.js') }}"></script>
<!-- CountDown  -->
<script src="{{ asset('js/jquery.countdown.js') }}"></script>
<!-- Validate form -->
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<!-- Smooth Scroll -->
<script src="{{ asset('js/smooth-scroll.js') }}"></script>
<!-- Youtube Player -->
<script src="{{ asset('js/jquery.youtubebackground.js') }}"></script>
<!-- Ajax chimp -->
<script src="{{ asset('js/jquery.ajaxchimp.js') }}"></script>
<!-- Google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAl-EDTJ5_uU3zBIX7-wNTu-qSZr1DO5Dw"></script>
<!-- jQuery Tweetie -->
<script src="{{ asset('js/tweetie.js') }}"></script>

<script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
<!-- Custom -->
<script src="{{ asset('js/main.js') }}"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176845985-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176845985-1');
</script>
    
<script type="text/javascript" src="https://cookieconsent.popupsmart.com/src/js/popper.js"></script><script> window.start.init({Palette:"palette5",Mode:"banner bottom",Time:"15",})</script>
</body>
</html>