@extends('master')

@section('content')

    <!-- HEADER -->


    <a id="openNav">
        <div class="logointerno"><img src="{{ asset('images/etichetta.png') }}" alt=""></div>
    </a>

    <!-- END HEADER -->
    <div class="container-fluid">
        <!-- START Special results title -->
        <div class="row">
            <div class="col-md-12 special_results_title margin-bottom-20">
                <div class="row">
                    <div class="col-md-6 title-container">
                        <h1>SPECIAL RESULTS</h1>
                        <p>. ideas into products</p>
                        <p>. for every place</p>
                        <p>. a continous creativity</p>

                    </div>
                    <div class="col-md-6">
                        <img class="img-responsive" src="{{ asset('images/results/furniture-lab-1.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-1.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-2.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-3.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-4.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-5.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-5b.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-AROSA-Armchairs-7.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-ALISA-Armchairs-1.jpg') }}">
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-ALISA-Armchairs-3.jpg') }}">
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-ALISA-Armchairs-5.jpg') }}">
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-WANGEN-Armchairs-1.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-project-WANGEN-Armchairs-2.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-4 col-md-offset-1">
                        <img class="img-responsive" src="{{ asset('images/results/furniture-lab-2.jpg') }}">
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4 ">
                        <img class="img-responsive" src="{{ asset('images/results/furniture-lab-3.jpg') }}">
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-4.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-5.jpg') }}">
                    </div>

                    <div class="col-md-4 ">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-6.jpg') }}">
                    </div>

                    <div class="col-md-4 ">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-7.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-20.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-15.jpg') }}">
                    </div>

                    <div class="col-md-4 ">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-21.jpg') }}">
                    </div>

                    <div class="col-md-4 ">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-16.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-4 col-md-offset-2">
                        <img style="float: left" class="img-responsive"
                             src="{{ asset('images/results/furniture-lab-special-results-9.jpg') }}">
                    </div>

                    <div class="col-md-4 ">
                        <img style="float: rigth;" class="img-responsive"
                             src="{{ asset('images/results/furniture-lab-special-results-10.jpg') }}">
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-11.jpg') }}">
                    </div>
                    <div class="col-md-12">
                        <p class="what-we-do-text text-center bellevue">
                            art. Bellevue 02 armchair by Very Wood
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-12.jpg') }}">
                    </div>

                    <div class="col-md-12 text-center">
                        <p class="what-we-do-text text-center special_results_loden">art. Loden armchair by Very
                            Wood</p>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-14.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-8.jpg') }}">
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-18.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-19.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-22.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/furniture-lab-special-results-23.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 margin-bottom-20">
                <div class="row">
                    <div class="col-md-12">
                        <img class="img-responsive center-block"
                             src="{{ asset('images/results/Furniture-Lab-Progetto-Arosa-2025.jpg') }}">
                    </div>
                </div>
            </div>
        </div>


    </div>

    @include('partials.footer')

@endsection

