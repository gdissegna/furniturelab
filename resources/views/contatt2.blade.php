@extends('master')

@section('content')

    <div class="row">
        <div class="col-md-5 col-md-offset-1 contact-container">
            <h1>
                Furniture Lab di Laura Purinan & C. sas
            </h1>
            <p>
                Via I° Maggio, 7/5<br>
                33040 Corno di Rosazzo (UD)<br>
                Cell. +39 338 4271412<br>
                Ph. +39 0432 753485<br>

                Email: <a href="mailto:info@furniturelab.it">info@furniturelab.it</a><br>
                Web: www.furniturelab.it<br>
            </p>
        </div>
        <div class="col-md-5">
            <img src="{{ asset('images/contact/furniturelab-contact-us-1.jpg') }}">
        </div>
        <div class="col-md-1">

        </div>
    </div>



    @include('partials.footer')
@endsection
