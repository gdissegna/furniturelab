@extends('master')

@section('content')

    <!-- HEADER -->


    <a id="openNav">
        <div class="logointerno"><img src="{{ asset('images/etichetta.png') }}"  alt=""> </div>
    </a>

    <!-- END HEADER -->

    <div class="container">

        <h1 class="main_title">Notices</h1>
        <div class="singular">
            <p>Si comunica ai sensi della L. 124/2017 di aver ricevuto, nel corso dell'anno 2023, il seguente contributo</p>
            <p><strong>Ente erogatore</strong></p>
            <p>Regione Friuli Venezia Giulia</p>
            <p><strong>Importo</strong></p>
            <p>Euro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12452,72</p>
            <p><strong>Determinazione di liquidazione</strong></p>
            <p>Dirigente CCIAA Pordenone - Udine n. 291 del 05.05.2022</p>
            <p><strong>Legge di riferimento</strong></p>
            <p>Legge Regionale FVG 11/2011</p>
            
                

    </div>


@endsection