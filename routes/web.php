<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
$router->get('/', function () use ($router) {
    return $router->app->version();
});
*/

$router->get('/', ['as' => 'home', 'uses' => 'StaticPagesController@home']);
$router->get('/what-we-do', ['as' => 'what-we-do', 'uses' => 'StaticPagesController@what_we_do']);
$router->get('/special-results', ['as' => 'special-results', 'uses' => 'StaticPagesController@special_results']);
$router->get('/materials', ['as' => 'materials', 'uses' => 'StaticPagesController@materials']);
$router->get('/contact', ['as' => 'contact', 'uses' => 'StaticPagesController@contact']);
$router->get('/privacy', ['as' => 'privacy', 'uses' => 'StaticPagesController@privacy']);
$router->get('/cookie', ['as' => 'cookie', 'uses' => 'StaticPagesController@cookie']);
$router->get('/notices', ['as' => 'notices', 'uses' => 'StaticPagesController@notices']);

