<?php

namespace App\Http\Controllers;

class StaticPagesController extends Controller
{

    private $View;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function home()
    {
        return View('home');
    }

    public function what_we_do()
    {
        return View('what-we-do');
    }

    public function special_results()
    {
        return View('special-results');
    }

    public function materials()
    {

    }

    public function contact()
    {
        return View('contatti');
    }

    public function privacy()
    {
        return View('privacy');
    }

    public function cookie()
    {
        return View('cookie');
    }

    public function notices()
    {
        return View('notices');
    }

}
